package test;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import org.junit.Test;


public class Main {

	private static String driver;
	private static String url;
	private static String username;
	private static String password;
	
	static {
		try {
			InputStream is = Main.class.getResourceAsStream("/jdbc.properties");
			Properties prop = new Properties();
			prop.load(is);
			
			driver = prop.getProperty("jdbc.driver");
			url = prop.getProperty("jdbc.url");
			username = prop.getProperty("jdbc.username");
			password = prop.getProperty("jdbc.password");
			
			// 将JDBC驱动加载到JVM中
			Class.forName(driver);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 演示JDBC
	 * */
	@Test
	public void insert() throws Exception {
		String sql = "insert into tb_user values(null,?,?)";
		Connection conn = DriverManager.getConnection(url, username, password);
		PreparedStatement pst = conn.prepareStatement(sql);
		pst.setString(1, "李四");
		pst.setInt(2, 30);
		
		pst.executeUpdate();
		
		pst.close();
		conn.close();
	}
	
	/**
	 * 查询
	 * */
	@Test
	public void query() throws Exception {
		String sql = "select * from tb_user where id>=?";
		Connection conn = DriverManager.getConnection(url, username, password);
		PreparedStatement pst = conn.prepareStatement(sql);
		pst.setInt(1, 1);
		
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");
			int age = rs.getInt("age");
			System.out.printf("id:%d, name:%s, age:%d\n", id, name, age);
		}
		
		rs.close();
		pst.close();
		conn.close();
	}
	
	
	
	
	/**
	 * 获得文件的路径
	 * */
	@Test
	public void test1() throws Exception {
		// 从类路径根目标查找
		String path = this.getClass().getClassLoader().getResource("jdbc.properties").toURI().getPath();
		System.out.println(path);
	}
	@Test
	public void test2() throws Exception {
		// 从当前目标查找，/表示根目录
		String path = this.getClass().getResource("/jdbc.properties").toURI().getPath();
		System.out.println(path);
	}
	
}
